package com.epam.rd.java.basic.task7.db;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private static final Logger logger = Logger.getLogger(DBManager.class.getName());

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> usersList = new ArrayList<>();
        try (Connection connection = getConnection(getConnectionUrl());
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQLQuery.FIND_ALL_USERS)) {

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                usersList.add(user);
            }

        } catch (SQLException e) {
            throw new DBException("can not find all users", e.getCause());
        }
        return usersList;
    }

    public boolean insertUser(User user) throws DBException {
        ResultSet resultSet;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            int index = 1;
            statement.setString(index, user.getLogin());
            if (statement.executeUpdate() > 0) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    user.setId(id);
                }
                return true;
            }

        } catch (SQLException e) {
            throw new DBException("can`t inset a new user", e.getCause());
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        boolean response = false;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement2 = connection.prepareStatement(SQLQuery.DELETE_USERS)) {
            for (User user : users) {
                statement2.setString(1, user.getLogin());
                if (statement2.executeUpdate() > 0) {
                    response = true;
                }
            }

        } catch (SQLException e) {
            throw new DBException("cannot delete users", e.getCause());
        }
        return response;
    }

    public User getUser(String login) throws DBException {
        ResultSet resultSet;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.GET_USER)) {
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                return user;
            }
        } catch (SQLException e) {
            throw new DBException("can`t find user by login", e.getCause());
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        ResultSet resultSet;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.GET_TEAM)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                return team;
            }
        } catch (SQLException e) {
            throw new DBException("can`t find team by name", e.getCause());
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamsList = new ArrayList<>();
        try (Connection connection = getConnection(getConnectionUrl());
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQLQuery.FIND_ALL_TEAMS)) {

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teamsList.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("can no find all teams", e.getCause());
        }
        return teamsList;
    }

    public boolean insertTeam(Team team) throws DBException {
        ResultSet resultSet;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            if (statement.executeUpdate() > 0) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    team.setId(id);
                }
                return true;
            }

        } catch (SQLException e) {
            throw new DBException("can`t insert a team", e.getCause());
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        boolean response = true;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection(getConnectionUrl());
            statement = connection.prepareStatement(SQLQuery.SET_TEAMS_FOR_USER, Statement.RETURN_GENERATED_KEYS);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            for (Team team : teams) {
                int count = 1;
                statement.setString(count++, Integer.toString(user.getId()));
                statement.setString(count++, Integer.toString(team.getId()));
                if (statement.executeUpdate() <= 0) {
                    response = false;
                }
            }
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            try {
                assert statement!= null;
                statement.close();
                connection.rollback();
            } catch (SQLException ex) {
                logger.warning("transaction failed in setTeamsForUser()");
            }
            throw new DBException("can`t set teams fro users", e.getCause());
        }
        return response;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.GET_USER_TEAMS, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, Integer.toString(user.getId()));
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                userTeams.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("no such user here hence no teams", e.getCause());
        } finally {
            closeConnection(resultSet);
        }
        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean response = false;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.DELETE_TEAM)) {

            statement.setInt(1, team.getId());
            if (statement.executeUpdate() > 0) {
                response = true;
            }

        } catch (SQLException e) {
            throw new DBException("can`t delete the team", e.getCause());
        }
        return response;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean response = false;
        try (Connection connection = getConnection(getConnectionUrl());
             PreparedStatement statement = connection.prepareStatement(SQLQuery.UPDATE_TEAM)) {

            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            if (statement.executeUpdate() > 0) {
                response = true;
            }
        } catch (SQLException e) {
            throw new DBException("oops something went wrong!\ncan`t update the team", e.getCause());
        }
        return response;
    }

    public String getConnectionUrl() {
        String url = null;
        Properties properties = new Properties();
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream("app.properties"))) {
            properties.load(bis);
            url = properties.getProperty("connection.url");
        } catch (FileNotFoundException e) {
            logger.warning("File not found!");
        } catch (IOException e) {
            logger.warning("Property file data may be incorrect");
        }
        return url;
    }

    public Connection getConnection(String connectUrl) throws SQLException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(connectUrl);
        } catch (SQLException e) {
            logger.warning("Connection to DataBase is failed");
        }
        return connection;
    }

    private void closeConnection(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
